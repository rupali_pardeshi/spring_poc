package com.example.demo.service

import com.example.demo.repository.UserLoginRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.stereotype.Service

@Service
class UserLoginService @Autowired constructor(private val userLoginRepository: UserLoginRepository) :
        UserDetailsService {

    override fun loadUserByUsername(username: String): UserDetails {
        return userLoginRepository.findByUserName(username)
    }

}