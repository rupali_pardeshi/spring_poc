package com.example.demo.model

import org.springframework.security.crypto.password.PasswordEncoder


data class RegistrationForm(val userName:String, val password : String){

    fun toUserLogin(passwordEncoder: PasswordEncoder): UserLogin {
        // TODO Id ??
       return UserLogin(1,userName,passwordEncoder.encode(password))
    }
}