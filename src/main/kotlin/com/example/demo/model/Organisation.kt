package com.example.demo.model

import lombok.AllArgsConstructor
import lombok.Data
import lombok.NoArgsConstructor
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name ="organisation")
data class Organisation (@Id @Column(name = "org_id") val orgId: Int,
                         @Column(name = "org_name") val orgName : String)
