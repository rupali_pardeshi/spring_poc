package com.example.demo.model

import lombok.AccessLevel
import lombok.Data
import lombok.NoArgsConstructor
import lombok.RequiredArgsConstructor
import javax.persistence.*

@Data
@RequiredArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
@Entity
@Table(name = "account")
data class User (@Id @Column(name = "user_id") val userId: Int,
                 @Column(name = "username") val userName: String,
                 val email : String,
                 @OneToOne
                 @JoinColumn(name = "org_id",referencedColumnName = "org_id" ) val organisation: Organisation)