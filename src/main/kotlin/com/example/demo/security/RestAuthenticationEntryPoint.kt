package com.example.demo.security

import org.springframework.security.core.AuthenticationException
import org.springframework.security.web.AuthenticationEntryPoint
import org.springframework.stereotype.Component
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class RestAuthenticationEntryPoint : AuthenticationEntryPoint {
    override fun commence(
            request: HttpServletRequest?,
            response: HttpServletResponse?,
            authException: AuthenticationException?
    ) {

        // the authentication process may automatically trigger when an un-authenticated client tries to access a secured resource.
        // This redirects to a login page so that the user can enter credentials.
        // But for now we are just sending unauthorised
        response?.sendError(HttpServletResponse.SC_UNAUTHORIZED,
                "Unauthorized");

    }
}