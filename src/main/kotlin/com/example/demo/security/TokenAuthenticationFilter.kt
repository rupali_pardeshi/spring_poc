package com.example.demo.security

import com.example.demo.service.UserLoginService
import org.graalvm.compiler.lir.CompositeValue
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource
import org.springframework.stereotype.Component
import org.springframework.util.StringUtils
import org.springframework.web.filter.OncePerRequestFilter
import java.util.*
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class TokenAuthenticationFilter @Autowired constructor(private val jwtTokenProvider: JwtTokenProvider,
                                                       private val userLoginService: UserLoginService
) : OncePerRequestFilter() {


    override fun doFilterInternal(request: HttpServletRequest,
                                  response: HttpServletResponse,
                                  filterChain: FilterChain) {
        val jwt = getJwtFromRequest(request)


        // TODO clean up in kotlin
        jwt?.let {
            if (StringUtils.hasText(jwt) && jwtTokenProvider.validateToken(jwt)) {
                val userIdFromToken = jwtTokenProvider.getUserIdFromToken(it)
                //TODO if user is not present in repo throw unauthorised
                val userDetails = userLoginService.loadUserByUsername(userIdFromToken)
                val authentication = UsernamePasswordAuthenticationToken(userDetails, null, userDetails.authorities)
                authentication.details = WebAuthenticationDetailsSource().buildDetails(request)
                SecurityContextHolder.getContext().authentication = authentication
            }
        }
        filterChain.doFilter(request, response)
    }


    private fun getJwtFromRequest(request: HttpServletRequest): String? {
        val bearerToken = request.getHeader("Authorization")
        return if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
            bearerToken.substring(7, bearerToken.length)
        } else null
    }

}