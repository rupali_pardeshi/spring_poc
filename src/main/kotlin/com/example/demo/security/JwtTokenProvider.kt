package com.example.demo.security

import io.jsonwebtoken.*
import org.springframework.stereotype.Component
import java.security.Key
import java.util.*
import javax.crypto.spec.SecretKeySpec
import javax.xml.bind.DatatypeConverter

@Component
class JwtTokenProvider {

    //TODO clean up

    // TODO we will be having different secret keys for diff env
    private var secretKey: String = "0FCCF49E0B523B22C12960C681AA77E2E6931EFC418749CD84B08D7521D3A3FC"
    final var signatureAlgorithm = io.jsonwebtoken.SignatureAlgorithm.HS256
    //We will sign our JWT with our ApiKey secret
    final var apiKeySecretBytes: ByteArray = DatatypeConverter.parseBase64Binary(secretKey)
    var signingKey: Key = SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.jcaName)

    fun createJwtToken(): String? {
        var nowMillis = System.currentTimeMillis()
        var issuedTime = Date(nowMillis)
        var expirationTime = Date(nowMillis.plus(6000))


        return Jwts.builder().setId("d")
                .setIssuedAt(issuedTime)
                .setExpiration(expirationTime)
                .setSubject("auth")
                .setIssuer("TwinTax")
                .claim("username", "admin")
                .signWith(signingKey)
                .compact()

    }


    fun decodeJWT(jwt: String): Claims {
        //This line will throw an exception if it is not a signed JWS (as expected)
        return Jwts.parser()
                .setSigningKey(DatatypeConverter.parseBase64Binary(secretKey))
                .parseClaimsJws(jwt).body
    }

    fun getUserIdFromToken(token: String): String {
        val claims = Jwts.parser()
                .setSigningKey(secretKey)
                .parseClaimsJws(token)
                .body
        return (claims["username"]).toString()
    }

    fun validateToken(jwt: String): Boolean {
        try {
            Jwts.parser().setSigningKey(secretKey)
            return true
        } catch (ex: SignatureException) {
        } catch (ex: MalformedJwtException) {
        } catch (ex: ExpiredJwtException) {
            //TODO refresh the token after validating the user is still active
        } catch (ex: UnsupportedJwtException) {
        } catch (ex: IllegalArgumentException) {
        }
        return false
    }
}