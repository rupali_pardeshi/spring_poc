package com.example.demo.security

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.Authentication
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler
import org.springframework.security.web.savedrequest.HttpSessionRequestCache
import org.springframework.stereotype.Component
import javax.servlet.http.Cookie
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


@Component
class RequestAuthenticationSuccessHandler  @Autowired constructor(val jwtCreator: JwtTokenProvider)
    : SimpleUrlAuthenticationSuccessHandler() {
    private val requestCache = HttpSessionRequestCache()

    override fun onAuthenticationSuccess(
            request: HttpServletRequest?,
            response: HttpServletResponse?,
            authentication: Authentication?
    ) {

        // we can add the target redirection url and the
        // jwt logic hear after successful login

        val createJwtToken = jwtCreator.createJwtToken()
        response?.addCookie(Cookie("token", createJwtToken))
//        response.addHeader(SecurityConstants.TOKEN_HEADER, SecurityConstants.TOKEN_PREFIX + token);


    }
}