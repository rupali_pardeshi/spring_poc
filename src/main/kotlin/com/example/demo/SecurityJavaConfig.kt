package com.example.demo

import com.example.demo.security.JwtTokenProvider
import com.example.demo.security.RequestAuthenticationSuccessHandler
import com.example.demo.security.TokenAuthenticationFilter
import com.example.demo.service.UserLoginService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.AuthenticationEntryPoint
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter


@Configuration
@EnableWebSecurity
class SecurityJavaConfig @Autowired constructor(
        val jwtTokenProvider: JwtTokenProvider,
        val userLoginService: UserLoginService,
        val restAuthenticationEntryPoint: AuthenticationEntryPoint,
        val requestAuthenticationSuccessHandler: RequestAuthenticationSuccessHandler)
    : WebSecurityConfigurerAdapter() {

    @Throws(Exception::class)
    override fun configure(auth: AuthenticationManagerBuilder) {
        auth.userDetailsService(userLoginService)
                .passwordEncoder(encoder())
    }

    @Bean
    fun encoder(): PasswordEncoder {
        return BCryptPasswordEncoder()
    }

    @Bean
    fun tokenAuthenticationFilter(): TokenAuthenticationFilter {
        // TODO clean up as per kotlin
        return TokenAuthenticationFilter(jwtTokenProvider,userLoginService)
    }

    //  adding endpoint to have authentication
    @Throws(Exception::class)
    override fun configure(http: HttpSecurity) {
        http
//                .csrf().disable()
                .exceptionHandling()
                .authenticationEntryPoint(restAuthenticationEntryPoint)
                .and()
                .authorizeRequests()
                .antMatchers("/users/*").authenticated()
                .and()
                .formLogin()
                .successHandler(requestAuthenticationSuccessHandler)
                .and()
                .logout()

        http.addFilterBefore(tokenAuthenticationFilter(), UsernamePasswordAuthenticationFilter::class.java)

    }
}