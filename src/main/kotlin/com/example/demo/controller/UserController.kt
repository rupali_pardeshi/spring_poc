package com.example.demo.controller

import com.example.demo.errorHandler.RecordNotFoundException
import com.example.demo.model.User
import com.example.demo.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
class UserController @Autowired constructor(private val repository: UserRepository) {

    @GetMapping("/users/{id}")
    fun getUser(@PathVariable("id") userId: Int): User {
        return repository.findById(userId)
                .orElseThrow { throw RecordNotFoundException("User Not Found") }
    }

    @GetMapping("/users")
    fun getAllUser(): MutableList<User> {
        println("""............. ${BCryptPasswordEncoder().encode("admin")}""")
        return repository.findAll()
    }

    @GetMapping("/users/organisation")
    fun getUserWithOrganisations(@RequestParam("org_id") organisationId : Int): List<User> {
        return repository.findByOrganisationOrgId(organisationId)
    }
}