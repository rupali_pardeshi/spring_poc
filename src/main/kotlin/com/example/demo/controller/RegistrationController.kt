package com.example.demo.controller

import com.example.demo.model.RegistrationForm
import com.example.demo.repository.UserLoginRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class RegistrationController @Autowired constructor(val userLoginRepository: UserLoginRepository,
                                                    val passwordEncoder: PasswordEncoder) {

    @PostMapping(value = ["/register"])
    fun registerForm(@RequestBody registrationForm: RegistrationForm) {
        userLoginRepository.save(registrationForm.toUserLogin(passwordEncoder))
    }
}