package com.example.demo.repository

import com.example.demo.model.UserLogin
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface UserLoginRepository : JpaRepository<UserLogin, Int>{
    fun findByUserName(userName: String):UserLogin
}
